# UndertowExample

#### 介绍
undertow替代tomcat应用在servlet、springboot

#### 安装教程

1. 安装jdk
2. 安装maven
3. 安装idea
4. 配置maven到idea

#### 使用说明

1. 项目克隆到本地
2. 使用idea打开项目
3. 测试时，若使用SpringBoot启动，则注释undertow的servlet依赖；若使用servlet启动器，则注释掉SpringBoot依赖