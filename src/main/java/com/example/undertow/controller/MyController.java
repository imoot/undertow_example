package com.example.undertow.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author imoot@gamil.com
 * @date 2019/5/22 0022 10:43
 */
@RestController
@RequestMapping("/myController")
public class MyController {

    @GetMapping("/index")
    public String index(){
        return "This is my first controller!";
    }

}
