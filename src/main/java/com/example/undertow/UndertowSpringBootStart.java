package com.example.undertow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author imoot@gamil.com
 * @date 2019/5/22 10:43
 */
@SpringBootApplication
public class UndertowSpringBootStart extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(UndertowSpringBootStart.class,args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(UndertowSpringBootStart.class);
    }
}
