package com.example.undertow.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author imoot@gamil.com
 * @date 2019/5/22 0022 10:09
 * 简单的servlet
 */
public class MyServlet extends HttpServlet {

    private static final long serialVersionUID = 2378494112650465478L;

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        writer.write("<p style='color:red;text-align:center;'>" + this.getInitParameter("message") + "</p>");
        writer.close();
    }

}
