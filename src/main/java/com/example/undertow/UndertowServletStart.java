package com.example.undertow;

/**
 * @author imoot@gamil.com
 * @date 2019/5/22 0022 9:57
 * servlet启动器
 */
public class UndertowServletStart {

    /*public static void main(final String[] args) {

        //创建ServletInfo，name必须唯一
        ServletInfo myInfo = Servlets.servlet("MyServlet",MyServlet.class);
        //初始化参数
        myInfo.addInitParam("message","This is my first Servlet!");
        //绑定映射
        myInfo.addMapping("/myServlet");

        //创建包部署对象，包含多个ServletInfo，近似为ServletInfo集合
        DeploymentInfo deploymentInfo = Servlets.deployment();
        //指定类加载器（即：指定启动类的类加载器）
        deploymentInfo.setClassLoader(UndertowServletStart.class.getClassLoader());
        //应用上下文（必须与映射路径一直，否则sessionId会出问题，每次都会新建）
        deploymentInfo.setContextPath("/undertow");
        //设置部署包名
        deploymentInfo.setDeploymentName("UndertowExample.war");
        //添加ServletInfo
        deploymentInfo.addServlets(myInfo);

        //使用默认的Servlet容器，并将部署添加到容器中，一个容器可以添加多个部署
        ServletContainer container = Servlets.defaultContainer();
        //将部署添加到容器，并生成对应的容器管理对象
        DeploymentManager manager = container.addDeployment(deploymentInfo);
        //实施部署
        manager.deploy();

        //分发器，将用户请求分发给对应的HttpHandler
        PathHandler handler = Handlers.path();
        //servlet path 处理器，部署管理对象启动后返回的Servlet处理器
        HttpHandler myapp = null;
        try {
            myapp=manager.start();
        } catch (ServletException e) {
            throw new RuntimeException("容器启动失败！");
        }

        //绑定映射关系
        handler.addPrefixPath("/undertow",myapp);

        Undertow server = Undertow.builder()
                //绑定端口和主机
                .addHttpListener(8080, "localhost")
                //设置分发处理器
                .setHandler(handler).build();
        //启动
        server.start();
    }*/

}
